package com.jtcoding.springbootmultidatasource.dao.first;

import com.jtcoding.springbootmultidatasource.model.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jason.tang
 * @create 2019-02-13 14:08
 * @description
 */

@Mapper
public interface UserDao {

    User getUser(Integer userNum);

    int deleteUser(Integer userNum);

    int addUser(User user);
}