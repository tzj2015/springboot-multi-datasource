package com.jtcoding.springbootmultidatasource.dao.second;

import com.jtcoding.springbootmultidatasource.model.SrcUser;
import com.jtcoding.springbootmultidatasource.model.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jason.tang
 * @create 2019-02-13 14:08
 * @description
 */
@Mapper
public interface SrcUserDao {

    SrcUser getSrcUser(Integer userNum);

    int deleteSrcUser(Integer userNum);

    int addSrcUser(User user);
}