package com.jtcoding.springbootmultidatasource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
public class SpringbootMultiDatasourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMultiDatasourceApplication.class, args);
    }

}

